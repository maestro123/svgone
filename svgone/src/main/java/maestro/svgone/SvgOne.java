package maestro.svgone;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.widget.ImageView;

import java.util.HashMap;

import static java.lang.String.valueOf;

/**
 * Created by Artyom on 9/5/2015.
 */
public class SvgOne {

    public static float DPI;

    private static Resources resources;

    public static void initialize(Context context) {
        resources = context.getResources();
        DPI = resources.getDisplayMetrics().density;
    }

    private static HashMap<String, SvgDrawable> svgCache = new HashMap<String, SvgDrawable>();
    private static HashMap<String, SvgDrawable> svgMenuCache = new HashMap<String, SvgDrawable>();

    public static void applySVG(ImageView imageView, int id, int color) {
        String key = String.valueOf(id) + color;
        SvgDrawable svg = svgCache.get(id + color);
        if (svg == null) {
            svg = getDrawable(resources, id, color);
            svgCache.put(key, svg);
        }
        imageView.setImageDrawable(svg);
    }

    public static void applySVG(ImageView imageView, int id) {
        String key = valueOf(id);
        SvgDrawable svg = svgCache.get(id);
        if (svg == null) {
            svg = getDrawable(resources, id);
            svgCache.put(key, svg);
        }
        imageView.setImageDrawable(svg);
    }

    public static void applySVG(ImageView imageView, int id, float scale) {
        String key = valueOf(id) + scale;
        SvgDrawable svg = svgCache.get(id);
        if (svg == null) {
            svg = getDrawable(resources, id, scale);
            svgCache.put(key, svg);
        }
        imageView.setImageDrawable(svg);
    }

    public static void applySVG(ImageView imageView, int id, int color, float scale) {
        String key = valueOf(id) + color + scale;
        SvgDrawable svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, Color.BLACK, color, scale);
            svgCache.put(key, svg);
        }
        imageView.setImageDrawable(svg);
    }

    public static SVGMenuItem getMenuDrawable(int id, int color) {
        String key = valueOf(id) + color;
        SvgDrawable svg = svgMenuCache.get(key);
        if (svg == null) {
            svg = getMenuDrawable(resources, id, color);
            svgMenuCache.put(key, svg);
        }
        return (SVGMenuItem) svg;
    }

    public static SvgDrawable getDrawable(int id, int color) {
        String key = valueOf(id) + color;
        SvgDrawable svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, color);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SvgDrawable getDrawable(int id, int searchColor, int color) {
        String key = valueOf(id) + searchColor + color;
        SvgDrawable svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, searchColor, color);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SvgDrawable getDrawable(int id, int replaceColor, float scale) {
        String key = valueOf(id) + replaceColor + scale;
        SvgDrawable svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, Color.BLACK, replaceColor, scale);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SvgDrawable getDrawable(int id, float scale) {
        String key = valueOf(id) + scale;
        SvgDrawable svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, scale);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SvgDrawable getDrawable(int id) {
        String key = valueOf(id);
        SvgDrawable svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SvgDrawable getDrawable(int id, int searchColor, int replaceColor, float scale) {
        String key = valueOf(id + searchColor + replaceColor) + "sc=" + scale;
        SvgDrawable svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, searchColor, replaceColor, scale);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SVGMenuItem getMenuDrawable(Resources resources, int resourceId, int replaceColor) {
        SvgDrawable svg = getDrawable(resources, resourceId, Color.BLACK, replaceColor, 1f);
        SVGMenuItem item = new SVGMenuItem(svg.getPicture(), svg.getLocalBounds());
        item.setScale(DPI);
        return item;
    }

    public static SvgDrawable getDrawable(Resources resources, int resourceId) {
        return SVGParser.getSVGFromResource(resources, resourceId).setScale(DPI);
    }

    public static SvgDrawable getDrawable(Resources resources, int resourceId, int searchColor, int replaceColor) {
        return SVGParser.getSVGFromResource(resources, resourceId, searchColor, replaceColor).setScale(DPI);
    }

    public static SvgDrawable getDrawable(Resources resources, int resourceId, float scale) {
        return SVGParser.getSVGFromResource(resources, resourceId).setScale(scale);
    }

    public static SvgDrawable getDrawable(Resources resources, int resourceId, int replaceColor) {
        return getDrawable(resources, resourceId, Color.BLACK, replaceColor);
    }

    public static SvgDrawable getDrawable(Resources resources, int resourceId, int searchColor, int replaceColor, float scale) {
        return SVGParser.getSVGFromResource(resources, resourceId, searchColor, replaceColor).setScale(scale);
    }

}
