package maestro.svgone.sample;

import android.app.Application;

import maestro.svgone.SvgOne;

/**
 * Created by artsiombarouski on 7/9/17.
 */

public class SvgApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SvgOne.initialize(this);
    }

}