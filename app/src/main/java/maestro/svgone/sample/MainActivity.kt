package maestro.svgone.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import maestro.svgone.SvgOne

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        SvgOne.applySVG(image, R.raw.homer)
        SvgOne.applySVG(image2, R.raw.tiger)

    }

}